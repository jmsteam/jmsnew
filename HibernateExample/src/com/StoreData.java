package com;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class StoreData {
public static void main(String args[])
{
	Configuration cg=new Configuration();
	cg.configure("hibernate.cfg.xml");
	SessionFactory sessionFactory=cg.buildSessionFactory();
	Session session=sessionFactory.openSession();
	Employee employee=new Employee();
	employee.setId(2);
	employee.setFirstName("abcdef");
	//employee.setLastName("pqr");
	session.update(employee);
	
	Transaction transaction=session.beginTransaction();
	transaction.commit();
	session.close();
	System.out.println("Sucessfull");
	
	
}
}
